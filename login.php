<?php

/**
 * Файл login.php для не авторизованного пользователя выводит форму логина.
 * При отправке формы проверяет логин/пароль и создает сессию,
 * записывает в нее логин и id пользователя.
 * После авторизации пользователь перенаправляется на главную страницу
 * для изменения ранее введенных данных.
 **/

// Отправляем браузеру правильную кодировку,
// файл login.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// Начинаем сессию.
session_start();

// В суперглобальном массиве $_SESSION хранятся переменные сессии.
// Будем сохранять туда логин после успешной авторизации.

if (!empty($_SESSION['login'])) {
    print('
    <form method="POST">
        <div class="d-grid gap-2 col-6 mx-auto">
            <input class="btn btn-light" type="submit" name="sessiondestroy" value="Выход"/>
        </div>
    </form>
    ');
}

if( isset( $_POST['sessiondestroy'] ) ) {
    session_destroy();
    header('Location: ./');
    exit();
}

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Lab 5</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
              rel="stylesheet"
              integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
              crossorigin="anonymous"
        >
    </head>
    <body>
    <div class="container">
        <form action="" method="post">
            <div class="mb-3">
                <label for="login" class="form-label">Ваш логин</label>
                <input type="login" class="form-control" name="login" id="login" aria-describedby="Enter your login">
            </div>
            <div class="mb-3">
                <label for="pwd" class="form-label">Ваш пароль</label>
                <input type="pwd" class="form-control" name="pwd" id="pwd">
                <div class="form-text">Ваш пароль узнают все. Наша корпорация не несёт никакой ответственности за ваши данные.</div>
            </div>
            <input type="submit" class="btn btn-primary" value="Войти"/>
        </form>
    </div>
    <footer>
        <h2 class="text-center m-3">
            Шустов В.Д. 25/2 2020-2021
        </h2>
    </footer>
    </body>
    </html>
    <?php
}
// Иначе, если запрос был методом POST, т.е. нужно сделать авторизацию с записью логина в сессию.
else {
    $user = 'shuimi';
    $pass = 'task1pass';
    $db = new PDO('mysql:host=localhost;dbname=study', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    try {
        $stmt = $db->prepare("SELECT pwd FROM userspass WHERE login=:i");
        $result = $stmt->execute(array("i"=>
            $_POST['login']
        ));
        $hashpwd = (current(current($stmt->fetchAll(PDO::FETCH_ASSOC))));
    }
    catch(PDOException $e) {
        print('Error : ' . $e->getMessage());
        exit();
    }

    if (empty($hashpwd)){
        print('<div class="alert alert-secondary" role="alert">Пользователь с таким именем или паролем не найден</div>');
    }
    else {
        if (password_verify($_POST['pwd'], $hashpwd)) {
            $_SESSION['uid'] = '12';
            $_SESSION['login'] = $_POST['login'];
            header('Location: ./');
            exit();
        } else {
            print('<div class="alert alert-secondary" role="alert">Неверный пароль</div>');
        }
    }

    exit();
}
?>
