<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>Lab 5</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
          crossorigin="anonymous">
    <style>
        .error {
            border:2px solid red;
        }
    </style>
</head>

<body>

<?php
if (!empty($messages)) {
    print('<div id="messages">');
    foreach ($messages as $message) {
        print($message);
    }
    print('</div>');
}
?>

<div class="container">
    <h1 class="text-center m-5">Web Backend Lab 5</h1>

    <div class="container-sm theme-list py-3 pl-0 mb-3">
        <div class="d-flex flex-column align-items-center">

            <form class="d-block p-2" action="" method="POST">

                <div class="form-floating mb-3">
                    <input name="name"
                        <?php if ($errors['name']) {print 'class="error form-control"';} else {print 'class="form-control"';} ?>
                           value="<?php print $values['name']; ?>"
                           type="text"
                    />
                    <label for="fio" class="form-label">Имя</label>
                </div>

                <div class="form-floating mb-3">
                    <input name="email"
                        <?php if ($errors['email']) {print 'class="error form-control"';} else {print 'class="form-control"';} ?>
                           value="<?php print $values['email']; ?>"
                            value="sample@example.com"
                            type="text"
                    />
                    <label for="email" class="form-label">Email</label>
                </div>

                <div class="mb-3">
                    <label for="year" class="form-label">Дата рождения</label>
                    <select name="year" <?php if ($errors['year']) {print 'class="error form-control"';} else {print 'class="form-control"';} ?>
                            value="<?php print $values['year']; ?>">
                        <option value="выбрать...">Выбрать</option>
                        <?php for($i = 1900; $i < 2021; $i++) {?>
                            <option <?php if ($values['year']==$i){print 'selected="selected"';} ?> value="<?php print $i; ?>"><?= $i; ?></option>
                        <?php }?>
                    </select>
                </div>

                <div class="mb-3">
                    <label <?php if ($errors['sex_error']) {print 'class="form-label error"';}else{print 'class="form-label"';} ?>>Пол</label>

                    <br>
                    <input type="radio"
                           name="sex"
                        <?php if ($values['sex']==0){print 'checked';} ?>
                           value="0"
                    /> Male

                    <input type="radio"
                        <?php if ($values['sex']==1){print 'checked';} ?>
                           name="sex"
                           value="1"
                    /> Female

                </div>

                <div class="mb-3">
                    <label class="form-label">Число конечностей</label>
                    <br>
                    <input type="radio"
                        <?php if ($values['limb']==0 || $values['limb']==1){print 'checked';} ?>
                           name="limb"
                           value="1"
                    />
                    <label class="form-check-label">1</label>
                        <input type="radio"
                            <?php if ($values['limb']==2){print 'checked';} ?>
                               name="limb"
                               value="2" />
                    <label class="form-check-label">2</label>
                    <input type="radio"
                        <?php if ($values['limb']==3){print 'checked';} ?>
                           name="limb"
                           value="3"
                    />
                    <label class="form-check-label">3</label>
                    <input type="radio"
                        <?php if ($values['limb']==4){print 'checked';} ?>
                           name="limb"
                           value="4" />
                    <label class="form-check-label">4</label>
                </div>

                <div class="mb-3">
                    <label class="form-label">Сверхсособности</label>
                    <select name="power[]" <?php if ($errors['power']) {print 'class="error form-control"';} else {print 'class="form-control"';} ?>
                            multiple="multiple">
                        <option
                            <?php if (in_array("0",$values['power'])){print 'selected="selected"';} ?>
                                value="god">Бессмертие
                        </option>
                        <option
                            <?php if (in_array("1",$values['power'])){print 'selected="selected"';} ?>
                                value="clip">Прохождение сквозь стены
                        </option>
                        <option
                            <?php if (in_array("2",$values['power'])){print 'selected="selected"';} ?>
                                value="fly">Левитация
                        </option>
                    </select>
                </div>

                <div class="mb-3 form-floating">
                    <textarea class="form-control"
                              name="bio"
                              placeholder="Your biography"
                              style="height: 100px; width: 420px;"><?php print $values['bio']; ?></textarea>
                    <label class="form-label">Биография</label>
                </div>

                <div class="mb-3 form-check">
                    <input type="checkbox" name="check" required/>
                    <label class="form-check-label">Даю согласие на всё!</label>
                </div>

                <div class="d-grid gap-2 col-6 mx-auto">
                    <button class="btn btn-light" type="submit" value="Отправить">Работать!</button>
                </div>

            </form>

        </div>
    </div>

</div>
<footer>
    <h2 class="text-center m-3">
        Шустов В.Д. 25/2 2020-2021
    </h2>
</footer>
</body>